package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controller {
    public Label monitor;
    public Button btn1;
    public Button btn2;
    public Button btn3;
    public Button btn4;
    public Button btn5;
    public Button btn6;
    public Button btn7;
    public Button btn8;
    public Button btn9;
    public Button btn0;
    public Float a = 0f , b = 0f ;
    public String Test = "";
    public Button btnC;
    public Button btnEqu;
    public Button btnAC;
    public Button btnMnlt;
    public Button btnDiv;
    public Button btn99;
    public Button btnSub;
    public Button btnPlus;

    public void doClick(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        String number = monitor.getText();
        monitor.setText(number.concat(button.getText()));
    }

    public void doCompute(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        switch (button.getText()){
            case "+" :
                Test = "+";
                a = Float.parseFloat(monitor.getText());
                monitor.setText("");
                break;
            case "-" :
                Test = "-";
                a = Float.parseFloat(monitor.getText());
                monitor.setText("");
                break;
            case "*" :
                Test = "*";
                a = Float.parseFloat(monitor.getText());
                monitor.setText("");
                break;
            case "/" :
                Test = "/";
                a = Float.parseFloat(monitor.getText());
                monitor.setText("");
                break;
            case "=" :
                b = Float.parseFloat(monitor.getText());
                switch (Test){
                    case "+":
                        monitor.setText(String.valueOf(a+b));
                        break;
                    case "-":
                        monitor.setText(String.valueOf(a-b));
                        break;
                    case "*":
                        monitor.setText(String.valueOf(a*b));
                        break;
                    case "/":
                        monitor.setText(String.valueOf(a/b));
                        break;
                }
                Test = "";
                break;
        }

    }

    public void doDelete(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        monitor.setText("");
        switch (button.getText()){
            case "AC":
                a = 0f;
                Test = "";
                break;
        }
    }
}
